#!/bin/bash

#Globals
SUFFIX_DIR="."
SUFFIX_LIST="https://publicsuffix.org/list/public_suffix_list.dat"
ZONE_F="/etc/bind/named.conf.local"
BIND_DIR="/etc/bind/"
NS="localhost"

if [ $UID -ne 0 ]; then
	echo "[!] Please run $(basename $0) with root rights"
	exit 1
fi

bind9-config --version &> /dev/null
if [ $? -ne 0 ]; then
	echo "[!] BIND9 is missing"
	exit 1
fi

kasutus() {
	echo -e "Usage:\n[-i <IP> -d <DOMAIN> or <IP> <DOMAIN>]\n[-f <FILE> or <FILE>] - Use a file as the input ( syntax: <IP> <DOMAIN> )\n[-h] - Displays this"
	exit 1
}

readfile() {
	while IFS='' read -r line || [[ -n "$line" ]]; do
		IP=$(echo $line | awk '{print $1}')
		DOM=$(echo $line | awk '{print $2}')
		validation $IP $DOM
		if [ $? -ne 0 ]; then
			continue
		fi
		addzone
		addrecord
	done < $FAIL
}

subdomain() {
	local dot_cnt=$(tr -cd "." <<< $1 | wc -m)
	for i in ${BIND_DIR}db.*; do
		local zonef=$(cut -d "." -f2- <<< $i)
		local i_dot_cnt=$(tr -cd "." <<< $zonef | wc -m)
		if [[ $dot_cnt -gt $i_dot_cnt ]]; then
			echo $1 | grep -q $zonef 2> /dev/null
			if [[ $? -eq 0 ]]; then
				MDOM=$zonef
				return 0
			fi
		fi
	done
	return 1
}

# for finding ccSLDs
sld() {
	if [[ ! -f $SUFFIX_DIR/public_suffix_list.dat ]]; then
		wget $SUFFIX_LIST &> /dev/null
	fi
	while IFS='' read -r line || [[ -n "$line" ]]; do
		if [[ $(cut -c 1 <<< $line) == "/" ]]; then
			continue
		elif [[ $(tr -cd "." <<< $line | wc -m) -eq 0 ]]; then
			continue
		fi
		if [[ $(tr -d '*' <<< $line) == $(echo $1 | rev | cut -d "." -f-2 | rev) ]]; then
			echo "[!] Second level hierarchy found"
			return 0
		fi
	done < <(grep -P "$1(?:[:\/]|$)" $SUFFIX_DIR/public_suffix_list.dat)
	return 1
}

addzone() {
	subdomain $DOM
	if [[ $? -eq 0 ]]; then
		echo "[!] Master domain already exists"
		return 2
	fi
	grep -iq $DOM $ZONE_F &>/dev/null
	if [ $? -eq 0 ]; then
		echo "[!] Zone file for $DOM already exists"
		return 1
	fi
	cat <<-EOF >> $ZONE_F
	zone "$DOM" {
		type master;
		file "/etc/bind/db.$DOM";
	};
	EOF
	cat <<-EOF >> "/etc/bind/db.$DOM"
	;
	; BIND data file for local loopback interface
	;
	\$TTL    604800
	@       IN      SOA     $NS. 	        contact.$DOM. (
                             $(date +"%s")  	; Serial
                             604800         	; Refresh
                              86400         	; Retry
                            2419200         	; Expire
                             604800 )       	; Negative Cache TTL
	;
	@       IN      NS      $NS.

	EOF
	echo "[!] Added $DOM zone"
}

addrecord() {
	sld $(echo $DOM | rev | cut -d "." -f-2 | rev ) $DOM
	if [[ $? -eq 0 ]]; then
		RECORD=$(echo $DOM | rev | cut -d "." -f4- | rev)
	else
		RECORD=$(echo $DOM | rev | cut -d "." -f3- | rev)
	fi

	if [ -z "$MDOM" ]; then
		local SEARCH=$(grep -i "@" ${BIND_DIR}db.$DOM 2> /dev/null | grep -E "[[:blank:]]A" 2> /dev/null)
		local syntax_type=0
		if [ -z "$SEARCH" ]; then
			local FULL_SEARCH=$(grep -E -ri "^$DOM." ${BIND_DIR}db.*)
			local S_LOC=$(cut -d ":" -f 1 <<< $FULL_SEARCH)
			local SEARCH=$(cut -d ":" -f 2 <<< $FULL_SEARCH)
			local syntax_type=1
		fi
	else
		local SEARCH=$(grep -i "$RECORD" ${BIND_DIR}db.$MDOM 2> /dev/null | grep -E "[[:blank:]]A" 2> /dev/null)
		local syntax_type=0
		if [[ $(echo $SEARCH | cut -d " " -f 1 | rev | cut -b 1) == "." ]]; then
			local FULL_SEARCH=$(grep -E -ri "^$DOM." ${BIND_DIR}db.*)
			local S_LOC=$(cut -d ":" -f 1 <<< $FULL_SEARCH)
			local SEARCH=$(cut -d ":" -f 2 <<< $FULL_SEARCH)
			local syntax_type=1
		fi
	fi

	local REC_TYPE=$(echo $SEARCH | awk '{print $3}')
	if [ ! -z "$REC_TYPE" ]; then
		echo "[!] Type $REC_TYPE record for $DOM already exists"
		while true; do
			read -p "~> Replace? ($(echo $SEARCH | xargs | cut -d: -f2-)): [(Y)es/(N)o/(A)dd a subdomain]: " RES < /dev/tty
			if [[ "$RES" == "Y" || "$RES" == "y" ]]; then
				if [ -z $MDOM ]; then
					if [ $syntax_type -eq 0 ]; then
						S_RES="${BIND_DIR}db.$DOM"
						sed -i "/@[[:blank:]]*IN[[:blank:]]*A[[:blank:]]*/d" $S_RES
					else
						S_RES=$S_LOC
						sed -i "/^$DOM./d" $S_RES
					fi
				else
					if [ $syntax_type -eq 0 ]; then
						S_RES="${BIND_DIR}db.$MDOM"
						sed -i "/$RECORD[[:blank:]]*IN[[:blank:]]*A[[:blank:]]*/d" $S_RES
					else
						S_RES=$S_LOC
						sed -i "/^$DOM./d" $S_RES
					fi
				fi
				increment_serial $S_RES
				rmreverse $DOM
				break
			elif [[ $RES == "N" || $RES == "n" ]]; then
				return 0
			elif [[ $RES == "A" || $RES == "a" ]]; then
				while true; do
					read -p "~> Enter a subdomain for $DOM: " SMB_DMN < /dev/tty
					OLD_DOM=$DOM
					DOM=${SMB_DMN}.${DOM}
					validation $IP $DOM
					if [[ $? -eq 0 && ! -z $SMB_DMN ]]; then
						addzone
						addrecord
						return 0
					else
						echo "[!] Invalid domain given"
						DOM=$OLD_DOM
						continue
					fi
				done
			fi
		done
	fi

	local IP_SEARCH=$(grep -ri "$IP" ${BIND_DIR}db.*)
	if [ ! -z "$IP_SEARCH" ]; then
		echo "[!] $IP is already in use"
		while true; do
			read -p "~> Replace? ($(echo $IP_SEARCH | xargs | cut -d: -f2-)): [(Y)es/(N)o]: " RES < /dev/tty
			if [[ $RES == "Y" || $RES == "y" ]]; then
				S_RES=$(echo $IP_SEARCH | cut -d':' -f 1)
				sed -i "/$IP/d" $S_RES
				increment_serial $S_RES
				rmreverse $(cut -d ':' -f-1 <<< ${IP_SEARCH#*db.})
				break
			elif [[ $RES == "N" || $RES == "n" ]]; then
				return 0
			fi
		done
	fi

	if [[ ! -z "$MDOM" ]]; then
		echo "$RECORD	IN	A	$IP" >> $BIND_DIR/db.$MDOM
		increment_serial ${BIND_DIR}db.$MDOM
		unset MDOM
	else
		echo "@		IN	A	$IP" >> $BIND_DIR/db.$DOM
		increment_serial ${BIND_DIR}db.$DOM
	fi
	echo "[!] Added A record for $DOM"
	addreverse
}

addreverse() {
	for i in {1..4}; do
		local IP_OCTET_$i=$(echo $IP | cut -d '.' -f $i)
	done
	RVR_S=$(grep "${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3}" $ZONE_F 2> /dev/null)
	if [ -z "$RVR_S" ]; then
		echo "[!] Creating reverse lookup zone for ${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3}"
		cat <<-EOF >> $ZONE_F
		zone "$IP_OCTET_3.$IP_OCTET_2.$IP_OCTET_1.in-addr.arpa" {
			type master;
			file "/etc/bind/db.${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3}";
		};
		EOF
	fi
	if [ ! -f ${BIND_DIR}db.${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3} ]; then
		cat <<-EOF > "/etc/bind/db.${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3}"
		;
		; BIND data file for local loopback interface
		;
		\$TTL    604800
		@       IN      SOA     $NS.            contact.$DOM. (
                             $(date +"%s")  	; Serial
                             604800         	; Refresh
                              86400         	; Retry
                            2419200         	; Expire
                             604800 )       	; Negative Cache TTL
		;
		@       IN      NS      $NS.

	EOF
	fi
	local DIR="${BIND_DIR}db.${IP_OCTET_1}.${IP_OCTET_2}.${IP_OCTET_3}"
	echo "$IP_OCTET_4		IN		PTR		$DOM." >> "$DIR"
	increment_serial "$DIR"
	echo "[!] Added reverse lookup for $DOM"
}

validation() {
	local param_ip=$1
	local status=1
	if [[ "$param_ip" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		OIFS=$IFS
		IFS='.'
		tested_ip=($param_ip)
		IFS=$OIFS
		[[ ${tested_ip[0]} -le 255 && ${tested_ip[1]} -le 255 \
				&& ${tested_ip[2]} -le 255 && ${tested_ip[3]} -le 255 ]]
		status=$?
	fi
	if [ $status -ne 0 ]; then
		echo "[!] Invalid IP: $1 has been supplied, skipping"
	fi
	if [[ $(tr -cd '.' <<< $2 | wc -m) -eq 0 ]]; then
		echo "[!] Invalid Domain: $2 has been supplied, skipping"
		status=1
	fi
	return $status
}

rmreverse() {
	local LOC=$(grep -nri $1 $BIND_DIR | grep PTR )
	if [ -z "$LOC" ]; then
		echo "[!] Unable to find PTR record for $1"
		return 1
	fi
	local RFILE=$(echo $LOC | cut -d ":" -f 1)
	local LINE=$(echo $LOC  | cut -d ":" -f 2 | awk '{print $1}')
	sed -i "${LINE}d" $RFILE 2> /dev/null
	return 0
}

restart_service() {
	systemctl restart bind9 || service bind9 restart 
	if [ $? -ne 0 ]; then
		echo "[!] Failed to restart bind9"
		exit 1
	else
		if [[ $(systemctl status bind9 | grep Active: | awk '{print $2}') == "active" ]]; then
			echo "[!] Successfully restarted BIND9"
			exit 0

		else 
			echo "[!] Failed to restart bind9"
			exit 1
		fi
	fi
}

increment_serial() {
	local SERIAL=$(cat $1 | grep -i Serial | awk '{print $1}')
	sed -i "s/$SERIAL/$(($SERIAL+1))/g" $1 &> /dev/null
	if [ $? -ne 0 ]; then
		echo "[!] Failed to increment the serial, check your configs"
		return 1
	fi
	return 0
}

no_flags() {
	if [[ ! -z "$1" && ! -z "$2" ]]; then
		IP=$1
		DOM=$2
		return 0
	fi
	return 1
}

filemode() {
	echo "[!] Reading from file"
	readfile
	restart_service
	exit 0
}

while getopts ":i:d:f:" arg; do
	case ${arg} in
		i ) IP=$OPTARG
		;;
		d ) DOM=$OPTARG
		;;
		f ) FAIL=$OPTARG
		;;
		\? ) echo "[!] Unknown flag: -$OPTARG"; exit 1
		;;
		: ) echo "[!] Argument needed for: -$OPTARG"; exit 1
		;;
		* ) kasutus
	esac
done

main() {
	if [[ -z "$DOM" || -z "$IP" ]]; then
		if [[ -r $1 ]]; then
			FAIL=$1
			filemode
		fi
		if [[ "$1" != "-f" ]]; then
			no_flags $1 $2
		fi
		if [[ "$?" -eq 0 ]]; then
			main 
		fi
		if [[ "$?" -ne 0 ]]; then
			exit 1
		fi
		if [[ -z "$FAIL" ]]; then
			kasutus
		elif [[ ! -r "$FAIL" ]]; then
			echo "[!] $FAIL isn't a valid file"
			exit 1
		else
			filemode
		fi
	else
		validation $IP $DOM
		if [ $? -eq 0 ]; then
			addzone
			addrecord
			restart_service
			exit 0
		else
			exit 1
		fi
	fi
}

main $@
