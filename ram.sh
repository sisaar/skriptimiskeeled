#!/bin/bash
read -p "Sisesta kasutajanimi: " USR
VSZ=$(ps -o vsz -u $USR --no-headers)
RSS=$(ps -o rss -u $USR --no-headers)
for i in $VSZ; do
let VSZ_USAGE+=$i
done
for j in $RSS; do
let RSS_USAGE+=$j
done
echo VSZ: $(numfmt --to=iec-i --suffix=B --padding=7 $(($VSZ_USAGE*1000)))
echo RSS: $(numfmt --to=iec-i --suffix=B --padding=7 $(($RSS_USAGE*1000)))
